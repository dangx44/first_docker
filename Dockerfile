FROM node:15
#recommended
WORKDIR /app
#. assumes app directory
COPY package.json .
#install dependencies in docker container
#RUN npm install
ARG NODE_ENV
RUN if [ "$NODE_ENV" = "development" ]; \
        then npm install; \ 
        else npm install --only=production; \
        fi
#Copy rest of file to docker image with . (current directory)
COPY . ./ 
#environment variables
ENV PORT 3000
#doesn't really effect anything, only for documentation
#EXPOSE 3000
EXPOSE $PORT
#Runtime, everything above is buildtime
CMD ["node", "index.js"]