const express = require("express");

const app = express();

//setup quick route for testing pruposes
app.get("/", (req, res) => {
  res.send("<h2>Hi there</h2>");
});

//If environment port setup, otherwise port 3000
const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`listening on port ${port}`));
